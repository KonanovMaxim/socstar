# Socstar Social Network

** Functionality: **

+ registration    
+ authentication  
+ ajax search with pagination  
+ display profile  
+ edit profile   
+ upload and download avatar  
+ users export to xml
+ send private/wall messages
+ add friends

** Tools: **  
JDK 7, Spring 4, JPA 2 / Hibernate 4, XStream, jQuery 2, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 8, PostgreSQL, IntelliJIDEA 16.  


** Notes: **  
SQL ddl is located in the `db/ddl.sql`

** Screenshots **
http://imgur.com/cXeBiZO
http://imgur.com/K16G1hG
http://imgur.com/gI1g4fo
http://imgur.com/FdErJcc
http://imgur.com/y9cNs6x
http://imgur.com/goaolJH
http://imgur.com/pIhGpKD

--  
**Конанов Максим**  
Тренинг getJavaJob,   
[http://www.getjavajob.com](http://www.getjavajob.com)