package com.getjavajob.konanov.socstar.service;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.FriendRequest;
import com.getjavajob.konanov.socstar.common.Phone;
import com.getjavajob.konanov.socstar.dao.AccountDao;
import com.getjavajob.konanov.socstar.dao.AccountDaoImpl;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 25.06.2016.
 */

@Service
public class AccountService {

    @Autowired
    private AccountDao<Account> accountDao = new AccountDaoImpl();

    public AccountService() {
    }

    public Account read(int id) throws ExceptionDao {
        Account account = accountDao.read(id);
        if (account != null) {
            account.setPhones(account.getPhones());
        } else {
            return null;
        }
        return account;
    }

    @Transactional
    public List<Account> filterByName(String filter) {
        List<Account> accounts = null;
        try {
            accounts = accountDao.filterByName(filter);
            for (Account account : accounts) {
                List<Account> accounts1 = account.getFriendsOut();
                accounts1.size();
            }
        } catch (ExceptionDao exceptionDao) {
            exceptionDao.printStackTrace();
        }
        return accounts;
    }

    @Transactional
    public void createAccount(Account account, String phoneStr) {
        Phone phone = new Phone(phoneStr, account);
        List<Phone> phones = new ArrayList<>();
        phones.add(phone);
        account.setPhones(phones);
        accountDao.create(account);
    }

    public Account readRegistered(String email, String password) {
        Account account = null;
        try {
            account = accountDao.readRegistered(email, password);
        } catch (ExceptionDao exceptionDao) {
            exceptionDao.printStackTrace();
        }
        return account;
    }

    @Transactional
    public void updateAccount(Account account) {
        accountDao.update(account);
    }

    @Transactional
    public void sendRequest(int account_id, Integer stalkedId) throws ExceptionDao {
        Account account = accountDao.read(account_id);
        Account newFriend = accountDao.read(stalkedId);
        List<Account> friends = account.getFriendsOut();
        if (!friends.contains(newFriend)) {
            FriendRequest friendRequest = new FriendRequest(account_id, stalkedId);
            accountDao.addFriendship(friendRequest);
        }
    }

    public void acceptRequest(int account_id, Integer acceptedId, Integer rejectedId) throws ExceptionDao {
        accountDao.acceptFriendship(account_id, acceptedId, rejectedId);
    }

    public FriendRequest.Status getFriendsStatus (int yourId, int friendsId) throws ExceptionDao {
        return accountDao.getFriendsStatus(yourId, friendsId);
    }

    public List<Account> allAccounts () throws ExceptionDao {
        return accountDao.allAccounts();
    }
}
