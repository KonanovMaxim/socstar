//package com.getjavajob.konanov.socstar.testservice;
//
//import com.getjavajob.konanov.socstar.common.Account;
//import com.getjavajob.konanov.socstar.common.Phone;
//import com.getjavajob.konanov.socstar.dao.MySQLAccountDao;
//import com.getjavajob.konanov.socstar.dao.MySQLPhoneDao;
//import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
//import com.getjavajob.konanov.socstar.service.AccountService;
//import org.junit.Assert;
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.runners.MockitoJUnitRunner;
//
//import java.io.FileNotFoundException;
//import java.util.ArrayList;
//import java.util.List;
//
//import static org.mockito.Matchers.*;
//
///**
// * Created by User on 28.07.2016.
// */
//
//@RunWith(MockitoJUnitRunner.class)
//public class AccountServiceTest {
//
//    @InjectMocks
//    private AccountService accountService;
//
//    @Mock
//    private MySQLAccountDao accountDao;
//
//    @Mock
//    private MySQLPhoneDao phoneDao;
//    private Account account;
//    private List<Phone> phones;
//    private Phone phone;
//
//    @Before
//    public void initialize() {
//        Account account = new Account("password", "new@gmail.com", "fedorov");
//        account.setId(1);
//        account.setFirstName("vasilij");
//
//        Phone phone = new Phone("89169238500", account);
//        phone.setId(1);
//        this.phone = phone;
//
//        List<Phone> phones = new ArrayList<>();
//        phones.add(phone);
//        this.phones = phones;
//
//        account.setPhones(phones);
//        this.account = account;
//    }
//
//    @Test
//    public void isValidAccountCreate() throws ExceptionDao {
//        Mockito.when(accountDao.createWithGeneratedKey(account)).thenReturn(1);
//        Mockito.when(accountDao.getGeneratedId()).thenReturn(1);
//
//        accountService.createAccount(account, "89169238500");
//        Mockito.verify(accountDao, Mockito.times(1)).createWithGeneratedKey(account);
//        Mockito.verify(accountDao, Mockito.times(1)).getGeneratedId();
//        Mockito.verify(phoneDao, Mockito.times(1)).create(any(Phone.class));
//    }
//
//    @Test
//    public void isInvalidAccountCreate() throws ExceptionDao {
//        Mockito.when(accountDao.createWithGeneratedKey(account)).thenReturn(0);
//        Mockito.when(accountDao.getGeneratedId()).thenReturn(0);
//
//        accountService.createAccount(account, "89169238500");
//        Mockito.verify(accountDao, Mockito.times(1)).createWithGeneratedKey(account);
//        Mockito.verify(accountDao, Mockito.times(1)).createWithGeneratedKey(account);
//        Mockito.verify(phoneDao, Mockito.times(0)).create(any(Phone.class));
//    }
//
//    @Test
//    public void getAllAccountsTest() throws FileNotFoundException, ExceptionDao {
//        accountService.filterByName();
//        Mockito.verify(accountDao, Mockito.times(1)).filterByName();
//    }
//
//    @Test
//    public void isValidAccountReadById() throws ExceptionDao {
//        Account testAcc = new Account("password", "new@gmail.com", "fedorov");
//        testAcc.setId(1);
//        testAcc.setFirstName("vasilij");
//
//        Mockito.when(accountDao.read(1)).thenReturn(testAcc);
//        Mockito.when(phoneDao.readByOwner(1)).thenReturn(phones);
//
//        Account receivedAct = accountService.read(1);
//        Mockito.verify(accountDao, Mockito.times(1)).read(anyInt());
//        Mockito.verify(phoneDao, Mockito.times(1)).readByOwner(anyInt());
//
//        Assert.assertEquals(account, receivedAct);
//    }
//
//    @Test
//    public void isInvalidAccountReadById() throws ExceptionDao {
//        Mockito.when(accountDao.read(1)).thenReturn(null);
//
//        Account receivedAct = accountService.read(1);
//        Mockito.verify(accountDao, Mockito.times(1)).read(anyInt());
//        Mockito.verify(phoneDao, Mockito.times(0)).readByOwner(anyInt());
//
//        Assert.assertNull(receivedAct);
//    }
//
//    @Test
//    public void isValidAccountReadByEmail() throws ExceptionDao {
//        Account testAcc = new Account("password", "new@gmail.com", "fedorov");
//        testAcc.setId(1);
//        testAcc.setFirstName("vasilij");
//
//        Mockito.when(accountDao.readRegistered("new@gmail.com", "password")).thenReturn(testAcc);
//        Mockito.when(phoneDao.readByOwner(1)).thenReturn(phones);
//
//        Account receivedAct = accountService.readRegistered("new@gmail.com", "password");
//        Mockito.verify(accountDao, Mockito.times(1)).readRegistered(anyString(), anyString());
//        Mockito.verify(phoneDao, Mockito.times(1)).readByOwner(anyInt());
//
//        Assert.assertEquals(account, receivedAct);
//    }
//
//    @Test
//    public void isInvalidAccountReadByEmail() throws ExceptionDao {
//        Account testAcc = new Account("password", "new@gmail.com", "fedorov");
//        testAcc.setId(1);
//        testAcc.setFirstName("vasilij");
//
//        Mockito.when(accountDao.readRegistered("new@gmail.com", "password")).thenReturn(null);
//
//        Account receivedAct = accountService.readRegistered("new@gmail.com", "password");
//        Mockito.verify(accountDao, Mockito.times(1)).readRegistered(anyString(), anyString());
//        Mockito.verify(phoneDao, Mockito.times(0)).readByOwner(anyInt());
//
//        Assert.assertNull(receivedAct);
//    }
//
//    @Test
//    public void isUpdated() throws ExceptionDao {
//        accountService.updateAccount(account);
//        Mockito.verify(accountDao, Mockito.times(1)).update(account);
//        Mockito.verify(phoneDao, Mockito.times(1)).update(phone);
//
//    }
//}
