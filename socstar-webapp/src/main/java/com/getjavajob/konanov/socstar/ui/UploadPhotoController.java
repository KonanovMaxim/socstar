package com.getjavajob.konanov.socstar.ui;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import com.getjavajob.konanov.socstar.service.AccountService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;

/**
 * Created by User on 01.08.2016.
 */

@Controller
public class UploadPhotoController {

    @Autowired
    ServletContext servletContext;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String uploadFileHandler(@SessionAttribute int account_id, Model model,
                             @RequestParam("file") MultipartFile file) throws ExceptionDao {
        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
                Account account = accountService.read(account_id);
                account.setPhoto(bytes);
                accountService.updateAccount(account);
                model.addAttribute("account", accountService.read(account_id));
                return "account";
            } catch (Exception e) {
                return "You failed to upload " + file.getName() + " => " + e.getMessage();
            }
        } else {
            return "You failed to upload " + file.getName()
                    + " because the file was empty.";
        }
    }

    //todo поставить рид комитед

    @RequestMapping(value = "/showPhoto", method = RequestMethod.GET)
    public void showPhoto(@SessionAttribute int account_id, HttpSession session,
                          HttpServletResponse response) throws ExceptionDao {
        if (session.getAttribute("stalkedId") != null) {
            if (accountService.read((int) session.getAttribute("stalkedId")) != null) {
                account_id = (int) session.getAttribute("stalkedId");
            }
        }
        Account account = accountService.read(account_id);
        byte[] content = account.getPhoto();
        doWritePhoto(response, content);
    }

    @RequestMapping(value = "/deletePhoto", method = RequestMethod.GET)
    public String deletePhoto(@SessionAttribute int account_id) throws ExceptionDao, FileNotFoundException {
        Account account = accountService.read(account_id);
        account.setPhoto(null);
        accountService.updateAccount(account);
        return "account";
    }

    @RequestMapping(value = "/writeFriendsPhoto", method = RequestMethod.GET)
    public void writeFriendsPhoto(@RequestParam int friend,
                          HttpServletResponse response) throws ExceptionDao {
        Account account = accountService.read(friend);
        byte[] content = account.getPhoto();
        doWritePhoto(response, content);
    }

    private void doWritePhoto(HttpServletResponse response, byte[] content) {
        if (content != null) {
            try {
                response.setContentType("image/jpeg");
                response.setContentLength(content.length);
                response.getOutputStream().write(content);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            try {
                InputStream avatar = servletContext.getResourceAsStream("/resources/images/avatar.png");
                response.getOutputStream().write(IOUtils.toByteArray(avatar));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
