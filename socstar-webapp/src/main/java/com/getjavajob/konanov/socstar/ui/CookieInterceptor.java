package com.getjavajob.konanov.socstar.ui;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by RUKONANOVM on 27/07/16.
 */

@Controller
public class CookieInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private AccountService accountService;

    @Override
    @Transactional
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        boolean onlySession = true;
        String email = null;
        String password = null;
        HttpSession session = request.getSession();
        if (request.getServletPath().contains("/resources")) {
            return true;
        }
        if (request.getServletPath().contains("/registration") ||
                request.getServletPath().contains("/login") || request.getServletPath().contains("/doLogin") ||
                request.getServletPath().contains("/doRegister")) {
            if (session.getAttribute("account_id") == null) {
                return true;
            } else {
                request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/account.jsp").forward(request, response);
                return false;
            }
        }
        if (session.getAttribute("account_id") == null) {
            Cookie[] cookies = request.getCookies();
            if (cookies != null) {
                for (Cookie cookie : cookies) {
                    if (cookie.getName().equals("email")) {
                        email = cookie.getValue();
                        onlySession = false;
                    }
                    if (cookie.getName().equals("password")) {
                        password = cookie.getValue();
                        onlySession = false;
                    }
                }
                if (onlySession) {
                    request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
                    return false;
                } else {
                    Account account = accountService.readRegistered(email, password);
                    List<Account> friendsIn = account.getFriendsIn();
                    List<Account> friendsOut = account.getFriendsIn();
                    List<Account> requests = account.getRequests();
                    friendsIn.size();
                    friendsOut.size();
                    requests.size();
                    session.setAttribute("friendsIn", friendsIn);
                    session.setAttribute("friendsOut", friendsOut);
                    session.setAttribute("requests", requests);
                    session.setAttribute("account_id", account.getId());
                    session.setAttribute("account", account);
                    request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/account.jsp").forward(request, response);
                    return false;
                }
            } else {
                request.getServletContext().getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
                return false;
            }
        }
        return true;
    }
}
