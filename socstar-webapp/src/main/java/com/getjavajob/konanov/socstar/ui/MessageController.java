package com.getjavajob.konanov.socstar.ui;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.Message;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import com.getjavajob.konanov.socstar.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * Created by RUKONANOVM on 16/08/16.
 */

@Controller
@RequestMapping(value = "/message/")
@SessionAttributes(value = {"account", "account_id", "friendsIn", "friendsOut", "requests"})
public class MessageController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "sendPrivateMessage", method = RequestMethod.POST)
    public ModelAndView sendPrivateMessage(@SessionAttribute int account_id,
                                     @RequestParam(value = "idToSend") Integer idToSend,
                                     @RequestParam String messageBody) throws ServletException, IOException, ExceptionDao {
        Message message = new Message(account_id, idToSend, messageBody, 300);
        accountService.sendPrivateMessage(message);
        ModelAndView mv = new ModelAndView("currentDialog");
        mv.addObject("currentDialog", accountService.openDialogById(account_id, idToSend));
        mv.addObject("withId", accountService.read(idToSend));
        return mv;
    }

    @RequestMapping(value = "showPrivateMessages", method = RequestMethod.GET)
    @ResponseBody
    public Set<Account> showListOfDialogs(@SessionAttribute int account_id) throws ExceptionDao {
        return accountService.showListOfDialogs(account_id);
    }

    @RequestMapping(value = "openDialog", method = RequestMethod.GET)
    public ModelAndView openDialog(@SessionAttribute int account_id,
                                    @RequestParam(value = "withId") Integer withId) throws ServletException, IOException, ExceptionDao {
        ModelAndView mv = new ModelAndView("currentDialog");
        mv.addObject("currentDialog", accountService.openDialogById(account_id, withId));
        mv.addObject("withId", accountService.read(withId));
        return mv;
    }
}
