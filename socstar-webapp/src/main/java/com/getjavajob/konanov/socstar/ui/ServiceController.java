package com.getjavajob.konanov.socstar.ui;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.FriendRequest;
import com.getjavajob.konanov.socstar.common.Message;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import com.getjavajob.konanov.socstar.service.AccountService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 26.07.2016.
 */

@Controller
@SessionAttributes(value = {"account", "account_id", "friendsIn", "friendsOut", "requests"})
public class ServiceController {

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String showRegistration() {
        return "registration";
    }

    @RequestMapping(value = "/doRegister", method = RequestMethod.POST)
    public ModelAndView doRegister(@RequestParam String lastName,
                                   @RequestParam String email,
                                   @RequestParam String password,
                                   @RequestParam String phone) {
        boolean regFail = false;
        if (lastName.trim().equals("Last name") || lastName.trim().equals("")) {
            regFail = true;
        }
        if (email.trim().equals("Email") || email.trim().equals("")) {
            regFail = true;
        }
        if (password.trim().equals("Password") || password.trim().equals("")) {
            regFail = true;
        }
        if (phone.trim().equals("Phone") || phone.trim().equals("")) {
            regFail = true;
        }
        if (regFail) {
            return new ModelAndView("registrationFailed");
        }
        Account account = new Account(password, email, lastName);
        accountService.createAccount(account, phone);
        if (accountService.readRegistered(email, password) != null) {
          ModelAndView mv = new ModelAndView("registrationSuccess");
          mv.addObject("last_name", lastName);
          return mv;
        } else {
            return null;
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String showLogin() {
        return "login";
    }

    @RequestMapping(value = "/doLogin", method = RequestMethod.GET)
    @Transactional
    public ModelAndView login(@RequestParam String email,
                              @RequestParam String password,
                              @RequestParam(defaultValue = "false") boolean cookies,
                              HttpServletResponse response, HttpSession session) {
        Account account = accountService.readRegistered(email, password);
        if (account == null) {
            ModelAndView mv = new ModelAndView("login");
            mv.addObject("errorMessage", "Email or password is incorrect. Try again please!");
            return mv;
        } else {
            if (cookies) {
                ModelAndView mv = new ModelAndView("account");
                Cookie emailCookie = new Cookie("email", email);
                Cookie passCookie = new Cookie("password", password);
                emailCookie.setMaxAge(60 * 20);
                emailCookie.setPath("/yourAccount");
                passCookie.setMaxAge(60 * 20);
                passCookie.setPath("/yourAccount");
                response.addCookie(emailCookie);
                response.addCookie(passCookie);
                List<Account> friendsIn = account.getFriendsIn();
                List<Account> friendsOut = account.getFriendsOut();
                List<Account> requests = account.getRequests();
                friendsIn.size();
                friendsOut.size();
                requests.size();
                session.setAttribute("friendsIn", friendsIn);
                session.setAttribute("friendsOut", friendsOut);
                session.setAttribute("requests", requests);
                session.setAttribute("account", account);
                session.setAttribute("account_id", account.getId());
                List<Message> wallMessages = accountService.openWall(account.getId());
                mv.addObject("wallMessages", wallMessages);
                return mv;
            } else {
                ModelAndView mv = new ModelAndView("account");
                session.setAttribute("account", account);
                session.setAttribute("account_id", account.getId());
                return mv;
            }
        }
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public ModelAndView logout(HttpServletRequest req, HttpServletResponse resp, SessionStatus sessionStatus) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession(false);
        if (session != null) {
            session.removeAttribute("JSESSIONID");
            sessionStatus.setComplete();
            session.invalidate();
        }
        Cookie[] cookies = req.getCookies();
        for (Cookie cookie : cookies) {
            cookie.setMaxAge(0);
            cookie.setValue(null);
            resp.addCookie(cookie);
        }
        return new ModelAndView("login");
    }

    @RequestMapping(value = "/searchAcc", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> autocompleteSearch(final @RequestParam("filter") String filter) {
        return accountService.filterByName(filter);
    }

    @RequestMapping(value = "/mainUtils", method = RequestMethod.GET)
    public String mainUtils() {
        return "mainUtils";
    }
}
