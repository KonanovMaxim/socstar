package com.getjavajob.konanov.socstar.ui;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.Phone;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import com.getjavajob.konanov.socstar.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by RUKONANOVM on 24/06/16.
 */

@Controller
@RequestMapping(value = "/account/")
@SessionAttributes(value = {"account", "account_id", "friendsIn", "friendsOut", "requests"})
public class AccountController {

    @Autowired
    ServletContext servletContext;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "allAccounts", method = RequestMethod.GET)
    public ModelAndView showAllAccounts() throws ServletException, IOException, ExceptionDao {
        List<Account> accounts = accountService.allAccounts();
        ModelAndView modelAndView = new ModelAndView("accounts");
        modelAndView.addObject("accounts", accounts);
        return modelAndView;
    }

    @RequestMapping(value = "update", method = RequestMethod.GET)
    public String showUpdate() throws ServletException, IOException {
        return "updateAccount";
    }

    @RequestMapping(value = "doUpdate", method = RequestMethod.POST)
    public String doUpdate(@SessionAttribute int account_id, Model model,
                                 @ModelAttribute Account accountToUpdate) throws ServletException, IOException, ExceptionDao {
        accountToUpdate.setId(account_id);
        accountService.updateAccount(accountToUpdate);
        model.addAttribute("account", accountService.read(account_id));
        return "updateAccount";
    }

    @RequestMapping(value = "updatePhones", method = RequestMethod.POST)
    public String updatePhones(@SessionAttribute int account_id, Model model,
                                     @RequestParam(defaultValue = "") String[] phone) throws ServletException, IOException, ExceptionDao {
        List<Phone> phones = new ArrayList<>();
        Account account = accountService.read(account_id);
        for (String aPhone : phone) {
            phones.add(new Phone(aPhone, account));
        }
        account.setPhones(phones);
        accountService.updateAccount(account);
        model.addAttribute("account", accountService.read(account_id));
        return "updateAccount";
    }

    @RequestMapping(value = "yourAccount")
    @Transactional
    public String account(HttpSession session, @SessionAttribute int account_id, Model model) {
        session.removeAttribute("stalkedId");
        Account account = accountService.read(account_id);
        updateCollections(model, account);
        model.addAttribute("account", accountService.read(account_id));
        model.addAttribute("wallMessages", accountService.openWall(account_id));
        return "account";
    }

    @RequestMapping(value = "mainHeader", method = RequestMethod.GET)
    public ModelAndView mainHeader() {
        return new ModelAndView("header");
    }

    @RequestMapping(value = "user{param}")
    @Transactional
    public String stalkAccount(@SessionAttribute int account_id, @PathVariable(value = "param") int param, HttpSession session, Model model) throws ExceptionDao {
        session.setAttribute("stalkedId", param);
        session.removeAttribute("friendsIn");
        session.removeAttribute("friendsOut");
        session.removeAttribute("requests");
        Account account = accountService.read(account_id);
        if (account.getRequestsFromMe().contains(accountService.read(param))) {
            model.addAttribute("isRequested", true);
        }
        session.setAttribute("isFriend", (account.getFriendsOut().contains(accountService.read(param))) ||
                account.getFriendsIn().contains(accountService.read(param)) ||
                account.getRequests().contains(accountService.read(param)));
        return "account";
    }

    @RequestMapping(value = "makeFriend")
    @Transactional
    public String makeFriend(@SessionAttribute int account_id,
                             @SessionAttribute(name = "stalkedId", required = false) Integer stalkedId, Model model) throws ExceptionDao {
        accountService.sendRequest(account_id, stalkedId);
        Account account = accountService.read(account_id);
        updateCollections(model, account);
        if (account.getRequestsFromMe().contains(accountService.read(stalkedId))) {
            model.addAttribute("isRequested", true);
        }
        model.addAttribute("account", account);
        return "account";
    }

    @RequestMapping(value = "acceptFriend")
    @Transactional
    public String makeFriend(@SessionAttribute int account_id,
                             @RequestParam(value = "acceptedId", required = false) Integer acceptedId,
                             @RequestParam(value = "rejectedId", required = false) Integer rejectedId, Model model) throws ExceptionDao {
        accountService.acceptRequest(account_id, acceptedId, rejectedId);
        Account account = accountService.read(account_id);
        updateCollections(model, account);
        model.addAttribute("account", accountService.read(account_id));
        return "account";
    }

    private void updateCollections(Model model, Account account) {
        List<Account> friendsIn = account.getFriendsIn();
        List<Account> friendsOut = account.getFriendsOut();
        List<Account> requests = account.getRequests();
        List<Account> requestsFromMe = account.getRequestsFromMe();
        friendsIn.size();
        friendsOut.size();
        requests.size();
        requestsFromMe.size();
        model.addAttribute("friendsIn", friendsIn);
        model.addAttribute("friendsOut", friendsOut);
        model.addAttribute("requests", requests);
        model.addAttribute("requestsFromMe", requestsFromMe);
    }
}
