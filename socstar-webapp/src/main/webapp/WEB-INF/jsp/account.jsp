<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/jquery-ui.css">
    <jsp:include page="header.jsp"/>
    <title>Account</title>
</head>
<body>
<div class="container text-center">
    <jsp:include page="leftMenu.jsp"/>


    <div class="col-sm-6">
        <c:if test="${stalkedId == null}">
            <ul id="personalInfo" class="list-group text-left">
                <h2 class="h2 text-center">Personal data:</h2>
                <li class="list-group-item">${"Last name: ".concat(account.lastName)}</li>
                <li class="list-group-item">${"First name: ".concat(account.firstName)}</li>
                <li class="list-group-item">${"Middle name: ".concat(account.middleName)}</li>
                <li class="list-group-item">${"Phone: ".concat(account.phones.get(0).phone)}</li>
                <li class="list-group-item">${"Email: ".concat(account.email)}</li>
            </ul>
            <c:if test="${wallMessages != null}">
                <ul id="currentDialog" class="list-group text-left">
                    <h2 class="h2 text-center">Wall</h2>
                    <c:forEach var="message" items="${wallMessages}" varStatus="count">
                        <c:if test="${message.status.value == 200}">
                            <div class="col-sm-12">
                                <div class="well">${"-".concat(message.messageContent)}</div>
                            </div>
                        </c:if>
                    </c:forEach>
                </ul>
            </c:if>
        </c:if>
    </div>

    <jsp:include page="rightMenu.jsp"/>
    <script>
        $(function () {
            function runEffect() {
                $('#effect').toggle("slide", {direction: "left"}, 300);
            }

            $(document).ready(function () {
                $("#effect").hide();
            });

            $("#buttonText").on("click", function () {
                runEffect();
            });
        });

        $(document).ready(function () {
            $("#privateMessages").on("click", function () {
                $.get("${pageContext.request.contextPath}/mainUtils #messagesPrivate", function (data) {
                    $("#personalInfo").replaceWith(data);
                });
            });
        });
    </script>
    <script src="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/external/jquery/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/jquery-ui.min.js"></script>
</body>
</html>