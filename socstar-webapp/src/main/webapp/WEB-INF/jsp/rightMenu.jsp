<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-sm-3">
    <c:if test="${requests.size() != 0 && stalkedId == null}">
        <div class="col-sm-10 well">
            <div class="thumbnail">
                <p>Friend requests:</p>
                <c:forEach var="request" items="${requests}">
                    <div class="row"><img id="friendPhotoRequest"
                                          src="${pageContext.request.contextPath}/writeFriendsPhoto?friend= + ${request.id}"
                                          width="64" height="64" alt="My Logo"></div>
                    <div class="row"><label for="friendPhoto"><a
                            href="${pageContext.request.contextPath}/account/user + ${request.id}">${request.lastName.concat(" ").concat(request.firstName)}</a></label>
                    </div>
                    <div class="row">
                        <a href="${pageContext.request.contextPath}/account/acceptFriend?acceptedId= + ${request.id}">
                            <div class="btn bg-success"><span class="glyphicon glyphicon-thumbs-up">Accept</span>
                            </div>
                        </a>
                        <a href="${pageContext.request.contextPath}/account/acceptFriend?rejectedId= + ${request.id}">
                            <div class="btn bg-danger"><span class="glyphicon glyphicon-thumbs-down">Decline</span>
                            </div>
                        </a>
                    </div>
                </c:forEach></div>
        </div>
    </c:if>
</div>
