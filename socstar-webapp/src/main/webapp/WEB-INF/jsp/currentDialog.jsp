<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 25.08.2016
  Time: 15:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/jquery-ui.css">
    <title>Current Dialog</title>
    <jsp:include page="header.jsp"/>
</head>
<body>
<div class="container text-center">
    <jsp:include page="leftMenu.jsp"/>
    <div class="col-sm-6">
        <c:if test="${currentDialog != null}">
            <ul id="currentDialog" class="list-group text-left">
                <h2 class="h2 text-center">Dialog with ${withId.firstName}:</h2>
                <c:forEach var="message" items="${currentDialog}" varStatus="count">
                    <c:if test="${message.status.value == 300}">
                        <c:choose>
                            <c:when test="${message.sender == account.id}">
                                <li class="list-group-item">
                                    <div id="leftDialog" class="row">
                                        <div class="col-sm-6"><label for="leftDialog">Me:</label>
                                            <div>${"-".concat(message.messageContent)}</div>
                                        </div>
                                        <div class="col-sm-6"></div>
                                    </div>
                                </li>
                            </c:when>
                            <c:otherwise>
                                <li class="list-group-item">
                                    <div id="rightDialog" class="row">
                                        <div class="col-sm-6">
                                        </div>
                                        <div class="col-sm-6"><label for="rightDialog">${withId.firstName}:</label>
                                            <div>${"-".concat(message.messageContent)}</div>
                                        </div>
                                    </div>
                                </li>
                            </c:otherwise>
                        </c:choose>
                    </c:if>
                </c:forEach>
            </ul>
        </c:if>
        <form class="form-group"
                     action="${pageContext.request.contextPath}/message/sendPrivateMessage?idToSend= + ${withId.id}"
                     method="post">
                            <textarea name="messageBody" class="form-control" rows="3" placeholder="What's up?" required
                                      style="height: 100px;"></textarea>
        <input class="btn btn-success" type="submit" value="Send"
               style="margin-top: 5px; margin-left: 370px;">
    </form>
    </div>
    <jsp:include page="rightMenu.jsp"/>
    <script>
        $(function () {
            function runEffect() {
                $('#effect').toggle("slide", {direction: "left"}, 300);
            }

            $(document).ready(function () {
                $("#effect").hide();
            });

            $("#buttonText").on("click", function () {
                runEffect();
            });
        });

        $(document).ready(function () {
            $("#privateMessages").on("click", function () {
                $.get("${pageContext.request.contextPath}/mainUtils #messagesPrivate", function (data) {
                    $("#currentDialog").replaceWith(data);
                });
            });
        });
    </script>
    <script src="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
    <script src="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/external/jquery/jquery.js"></script>
    <script src="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/jquery-ui.min.js"></script>
</div>
</body>
</html>
