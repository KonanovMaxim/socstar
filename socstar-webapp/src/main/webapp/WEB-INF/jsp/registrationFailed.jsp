<%--
  Created by IntelliJ IDEA.
  User: RUKONANOVM
  Date: 05/07/16
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page session="false" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.css">
<script src="${pageContext.request.contextPath}/resources/jquery-3.1.0.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom_css/custom.css">
<script src="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<html>
<head>
    <title>Registration Success</title>
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-2"></div>
        <div class="col-lg-8">
            <div class="list-group-item-text">
                <div class="form-horizontal" style="margin-top: 50%"><h1 class="text-center" style="color: black; font-size: medium; font-weight: 600">
                    Sorry, you missed some data to input or did it wrong!</h1></div>
            </div>
        </div>
        <div class="col-lg-2"></div>
    </div>
</div>
</body>
</html>
