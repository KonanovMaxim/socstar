<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: RUKONANOVM
  Date: 01/08/16
  Time: 9:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom_css/custom.css">
<script src="${pageContext.request.contextPath}/resources/jquery-3.1.0.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<html>
<head>
    <title>Login</title>
</head>
<body>
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="${pageContext.request.contextPath}/resources/images/starmedium.png"/>
        <img id="profile-img-2" class="img-responsive" style="margin-left: 45px" src="${pageContext.request.contextPath}/resources/images/socstar.png"/>
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" action="${pageContext.request.contextPath}/doLogin" method="get">
            <div class="text-center" style="color: black; font-size: smaller; font-style: italic; font-weight: 600">${errorMessage}</div>
            <c:if test="${errorMessage!= null}" ><div class="text-center" style="color: black; font-size: small; font-style: italic; font-weight: 600"><a href='${pageContext.request.contextPath}/registration'>Registration</a></div></c:if>
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" id="inputEmail" name="email" class="form-control" placeholder="Email address" required autofocus>
            <input type="text" id="inputPassword" name="password" class="form-control" placeholder="Password" required>
            <div id="remember" class="checkbox">
                <label>
                    <input type="checkbox" title="Remember me" name="cookies" > Remember me
                </label>
            </div>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
        </form>
        <a href="#" class="forgot-password">
            Forgot the password?
        </a>
    </div>
</div>
</body>
</html>
