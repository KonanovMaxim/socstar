<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: RUKONANOVM
  Date: 01/08/16
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.css">
<script src="${pageContext.request.contextPath}/resources/jquery-3.1.0.js"></script>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom_css/custom.css">
<script src="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<html>
<head>
    <title>Registration</title>
</head>
<body>
<div class="container">
    <div class="card card-container">
        <img id="profile-img" class="profile-img-card" src="${pageContext.request.contextPath}/resources/images/starmedium.png"/>
        <img id="profile-img-2" class="img-responsive" style="margin-left: 45px" src="${pageContext.request.contextPath}/resources/images/socstar.png"/>
        <p id="profile-name" class="profile-name-card"></p>
        <form class="form-signin" action="${pageContext.request.contextPath}/doRegister" method="post">
            <h1 class="text-center" style="font-size: small; font-weight: 600">Are you hip enough?</h1>
            <span id="reauth-email" class="reauth-email"></span>
            <input type="text" name="lastName" class="form-control" placeholder="Last name" required autofocus>
            <input type="text" name="email"  class="form-control" placeholder="Email adress" required>
            <input type="text" name="password" class="form-control" placeholder="Password" required>
            <input type="text" name="phone" class="form-control" placeholder="Phone number" required>
            <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Register</button>
        </form>
    </div>
</div>
</body>
</html>
