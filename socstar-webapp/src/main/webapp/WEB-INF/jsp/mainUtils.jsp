<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: User
  Date: 16.08.2016
  Time: 20:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Main Utils</title>
</head>
<body>
<div id="messagesPrivate" class="col-sm-6" style="width: 550px;">
</div>
<script>
    $(document).ready(function () {
        $.getJSON('${pageContext.request.contextPath}/message/showPrivateMessages', {}, function (json) {
            $('#messagesPrivate').html('');
            $('#messagesPrivate').append('<h2 class="h2">' + 'Dialogs: ' + '</h2>');
            for (var i in json) {
                $('#messagesPrivate').append(
                        '<div class="well" style="background-color: #c7ddef;">' +
                        '<img class="img-circle" width="24" height="24" src="' +
                        '${pageContext.request.contextPath}/writeFriendsPhoto?friend=' +
                        json[i].id + '"/>' + " " +
                        '<a class="btn" href="' + "${pageContext.request.contextPath}/message/openDialog?withId=" + json[i].id + '">'
                        + json[i].lastName + " " + json[i].firstName + '</a>' + '</div>'
                );
            }
        });
    });
</script>
</body>
</html>
