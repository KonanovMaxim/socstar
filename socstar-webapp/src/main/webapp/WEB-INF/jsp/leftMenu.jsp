<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="col-sm-3">
    <div class="row"><img src="${pageContext.request.contextPath}/showPhoto" class="img-circle" height="265"
                          width="265"
                          alt="Avatar" style="margin-left: -150px;"></div>
    <c:if test="${stalkedId == null}">
        <div class="row" style="margin-top: 15px"><a href="${pageContext.request.contextPath}/deletePhoto"
                                                     style="margin-left: -150px;">Delete photo</a></div>
    </c:if>

    <!-- Add as friend button -->
    <c:if test="${stalkedId != null && !isFriend && !isRequested}">
        <div class="row">
            <div class="col-sm-12" style="margin-left: -75px;">
                <div class="panel-body">
                    <a href="${pageContext.request.contextPath}/account/makeFriend">
                        <button type="button" class="btn btn-default btn-danger">
                            <span class="glyphicon glyphicon-user"></span> Add friend
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </c:if>

    <!--Text your friend-->
    <c:if test="${stalkedId != null && isFriend}">
        <button id="buttonText" class="btn bg-primary ui-state-default ui-corner-all"
                style="margin-left: -150px;margin-top: 5px;">
            Message buddy
        </button>
        <div class="toggler" style="margin-top: 30px; margin-left: -110px">
            <div id="effect" class="ui-widget-content ui-corner-all">
                <h3 class="ui-widget-header ui-corner-all">Text friend</h3>
                <div class="form-horizontal">
                    <div class="form-group">
                        <form class="col-md-6"
                              action="${pageContext.request.contextPath}/message/sendPrivateMessage?idToSend= + ${stalkedId}"
                              method="post">
                            <textarea name="messageBody" class="form-control" rows="3" placeholder="What's up?" required
                                      style="margin-left: 12px;height: 100px; width: 340px;"></textarea>
                            <input class="btn btn-success" type="submit" value="Send"
                                   style="margin-top: 5px; margin-left: 270px;">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </c:if>

    <!-- Friends panel -->
    <c:if test="${stalkedId == null}">
        <div class="row" style="margin-top: 15px; margin-left: -25px;">
            <div class="well col-sm-6 ">
                <div class="row" style="margin-top: -25px"><h2><a class="alert alert-success"
                                                                  style="font-size: medium; font-weight: 600">Friends:</a>
                </h2></div>
                <c:if test="${(friendsIn.size() != 0)}">
                    <div class="col-sm-6 " style="margin-top: 15px">
                        <c:forEach var="friendIn" items="${friendsIn}" varStatus="index">
                            <c:if test="${index.index <= 2}">
                                <div class="row"><img id="friendPhoto" class="img-circle"
                                                      src="${pageContext.request.contextPath}/writeFriendsPhoto?friend= + ${friendIn.id}"
                                                      width="32" height="32" alt="My Logo"></div>
                                <div class="row"><label for="friendPhoto"><a
                                        style="font-weight: 300; font-size: smaller"
                                        href="${pageContext.request.contextPath}/account/user + ${friendIn.id}">${friendIn.lastName.concat(" ").concat(friendIn.firstName)}</a></label>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </c:if>

                <c:if test="${(friendsOut.size() != 0)}">
                    <div class="col-sm-6 " style="margin-top: 15px">
                        <c:forEach var="friendOut" items="${friendsOut}" varStatus="index">
                            <c:if test="${index.index <= 2}">
                                <div class="row"><img id="friendPhotoOut" class="img-circle"
                                                      src="${pageContext.request.contextPath}/writeFriendsPhoto?friend= + ${friendOut.id}"
                                                      width="32" height="32" alt="My Logo"></div>
                                <div class="row"><label for="friendPhoto"><a
                                        style="font-weight: 300; font-size: smaller"
                                        href="${pageContext.request.contextPath}/account/user + ${friendOut.id}">${friendOut.lastName.concat(" ").concat(friendOut.firstName)}</a></label>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </c:if>
            </div>
        </div>
    </c:if>
</div>
