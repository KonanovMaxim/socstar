<%--
  Created by IntelliJ IDEA.
  User: RUKONANOVM
  Date: 08/07/16
  Time: 10:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/custom_css/updateAcc.css">
<jsp:useBean id="phone" class="com.getjavajob.konanov.socstar.common.Phone"/>
<html>
<head>
    <jsp:include page="header.jsp"/>
    <title>UpdateAccount</title>
</head>
<body>
<div class="col-md-12">
    <div class="row">
        <div class="container-fluid" style="width: 400px; margin-left: 0">

            <div class="tab-panels">
                <ul class="tabs">
                    <li rel="panel1" class="active">Initials</li>
                    <li rel="panel2">Email/Skype/ICQ</li>
                    <li rel="panel3">Phones</li>
                    <li rel="panel4">Photo</li>
                </ul>

                <div id="panel1" class="panel active">
                    <form id="formField1" action="${pageContext.request.contextPath}/account/doUpdate" method="post">
                        <div class="form-group">
                            <label for="ln">Last name:</label>
                            <input type="text" value="${account.lastName}" name="lastName" class="form-control" id="ln">
                        </div>
                        <div class="form-group">
                            <label for="mn">Middle name:</label>
                            <input type="text" value="${account.middleName}" name="middleName" class="form-control"
                                   id="mn">
                        </div>
                        <div class="form-group">
                            <label for="fn">First name:</label>
                            <input type="text" value="${account.firstName}" name="firstName" class="form-control"
                                   id="fn">
                        </div>
                    </form>
                </div>

                <div id="panel2" class="panel">
                    <form id="formField2" action="${pageContext.request.contextPath}/account/doUpdate" method="post">
                        <div class="form-group">
                            <label for="em">Email:</label>
                            <input type="text" value="${account.email}" name="email" class="form-control" id="em">
                        </div>
                        <div class="form-group">
                            <label for="sk">Skype:</label>
                            <input type="text" value="${account.skype}" name="skype" class="form-control" id="sk">
                        </div>
                        <div class="form-group">
                            <label for="icq">ICQ:</label>
                            <input type="text" value="${account.icq}" name="icq" class="form-control" id="icq">
                        </div>
                    </form>
                </div>

                <div id="panel3" class="panel">
                    <form class="commentForm" method="post"
                          action="${pageContext.request.contextPath}/account/updatePhones">
                        <div>
                            <label for="phone">Phones:</label>
                            <div id="wrapper">
                                <div><c:forEach items="${account.phones}" var="phone" varStatus="count">
                                    <div class="form-group" id="group-${count.index}">
                                        <input type="text" value="${phone.phone}" id="phone" name="phone"
                                               class="form-control">
                                        <button type="button" class="btn btn-danger"
                                                onclick="registerEdit('${count.index}')">X
                                        </button>
                                    </div>
                                </c:forEach></div>
                            </div>
                            <input type="button" value="add" id="add_button"/>
                        </div>
                    </form>
                </div>

                <div id="panel4" class="panel">
                    <form method="POST" action="${pageContext.request.contextPath}/upload"
                          enctype="multipart/form-data">
                        File to upload: <input type="file" name="file">
                        <input type="submit" value="Upload"> Press here to upload the file!
                    </form>
                </div>

                <button class="btn btn-danger" type="button" name="btn" value="Submit" id="submitBtn"
                        data-toggle="modal" data-target="#confirm-submit">Save
                </button>


                <div class="modal fade" id="confirm-submit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel">Apply changes?</h4>
                            </div>
                            <div class="modal-body">Are you sure you want to modify your profile?</div>
                            <div class="modal-footer">
                                <button onclick="submitForms()" class="btn btn-success success">Yes
                                </button>
                                <button type="button" data-dismiss="modal"
                                        aria-hidden="true" class="btn btn-primary">No
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
<script src="${pageContext.request.contextPath}/resources/jquery-3.1.0.js"></script>
<script src="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/custom_js/updateAcc.js"></script>
</html>
