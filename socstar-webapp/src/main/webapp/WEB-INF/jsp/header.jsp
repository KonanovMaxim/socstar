<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: RUKONANOVM
  Date: 28/07/16
  Time: 10:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-3.3.5-dist/css/bootstrap.css"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/jquery-ui.min.css">
<script src="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/external/jquery/jquery.js"></script>
<script src="${pageContext.request.contextPath}/resources/jquery-ui-1.12.0/jquery-ui.min.js"></script>
<html>
<head>
    <title>header</title>
</head>
<style>
    .navbar-nav > li > a {
        line-height: 15px;
    }
</style>
<body>
<!-- Begin NavBar -->
<nav class="navbar navbar-inverse navbar-static-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <ul class="nav navbar-nav navbar-left">
            <li class="navbar-brand" style="margin-left: -60px"><img
                    src="${pageContext.request.contextPath}/resources/images/star.png"
                    width="32" height="32" alt="My Logo"></li>
            <li style="margin-left: -27px"><a class="navbar-link"
                                              href="${pageContext.request.contextPath}/account/yourAccount"><img
                    src="${pageContext.request.contextPath}/resources/images/socstar.png"
                    height="35" alt="My Logo"></a></li>
        </ul>
        <div class="navbar-collapse collapse menu2" style="margin-right: -60px">
            <ul class="nav navbar-nav navbar-right">
                <li class="active navbar-btn"><a class="btn btn-danger" href="${pageContext.request.contextPath}/logout"
                                                 style="background-color: rgba(213, 37, 28, 0.78)"><span
                        class="glyphicon glyphicon-off"></span><span> Logout</span></a>
                </li>
                <li class="navbar-btn"><a href="${pageContext.request.contextPath}/account/update"><span
                        class="glyphicon glyphicon-user"></span><span><c:out value="${account.lastName}"/>
                    <c:out value="${account.firstName}"/>
                    <c:out value="${account.middleName}"/></span></a></li>

                <li style="margin-right: -10px"><a><input id="accountSearch" type="text" class="form-control"
                                                          placeholder="Search"></a>
                </li>

                <li class="dropdown" style="margin-top: 15px;">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1"
                            data-toggle="dropdown" aria-expanded="true"
                            style="background-color: rgba(213, 37, 28, 0.78)">
                        Personal Stuff
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
                        <li id="privateMessages" role="presentation"><a role="menuitem" tabindex="-1" href="#"><span
                                class="glyphicon glyphicon-envelope"></span><span> Dialogs</span></a>
                        </li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><span
                                class="glyphicon glyphicon-"></span><span> Groups</span></a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                   href="${pageContext.request.contextPath}/account/allAccounts">Friends</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
<script>
    var getAcc = null;
    $('#accountSearch').autocomplete({
        minLength: 2,
        source: function (request, response) {
            getAcc = $.get('<c:url value="/searchAcc?filter=" />' + request.term, function (data) {
                response($.map(data, function (seekAccount, i) {
                    return {
                        //todo передавать в лэйбле все что нужно
                        value: seekAccount.lastName + " " + seekAccount.firstName,
                        pushToAcc: seekAccount.id,
                        label: seekAccount.lastName + " " + seekAccount.firstName
                    }
                }))
            })
        },
        focus: function (event, ui) {
            $('#accountSearch').val(ui.item.label);
            return false;
        },
        select: function (event, ui) {
            location.href = "${pageContext.request.contextPath}/account/user" + ui.item.pushToAcc;
        }
    });
</script>
</body>
</html>