<%--
  Created by IntelliJ IDEA.
  User: RUKONANOVM
  Date: 06/07/16
  Time: 8:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>showAccounts</title>
</head>
<body>
<h2>SocstaR</h2><p>List of enrolled accounts:</p>
<table BORDER=1 CELLPADDING=0 CELLSPACING=0 WIDTH=50% ><th>ID</th><th>First Name</th><th>email</th>
    <c:forEach var="account" items="${accounts}">
    <tr>
        <td>${account.id}</td>
        <td>${account.firstName}</td>
        <td>${account.email}</td>
        </tr>
        </c:forEach>
    </table>
</body>
</html>
