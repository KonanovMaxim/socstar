/**
 * Created by User on 10.08.2016.
 */
$(function () {

    $('.tab-panels .tabs li').on('click', function () {

        var $panel = $(this).closest('.tab-panels');

        $panel.find('.tabs li.active').removeClass('active');
        $(this).addClass('active');

        //figure out which panel to show
        var panelToShow = $(this).attr('rel');

        //hide current panel
        $panel.find('.panel.active').slideUp(300, showNextPanel);

        //show next panel
        function showNextPanel() {
            $(this).removeClass('active');

            $('#' + panelToShow).slideDown(300, function () {
                $(this).addClass('active');
            });
        }
    });
});

// Attach Button click event listener
$("#submitBtn").click(function () {

});

submitForms = function () {
    $('form:visible').submit();
};

$(document).ready(function () {
    var max_fields = 5;
    var wrapper = $("#phones")
    var add_button = $("#add_phone")

    var x = 1;
    $("#add_button").click(function (e) {
        e.preventDefault();
        if (x < max_fields) {
            x++;
            $("#wrapper").append('' +
                '<div style="padding-bottom: 5px;">' +
                '<input class="form-control" type="text" name="phone" value="0"/>' +
                '<button type="button" class="btn btn-danger" id="remove_phone">&ndash;</button>' +
                '</div>');
        }
    });

    $("#wrapper").on("click", "#remove_phone", function (e) {
        e.preventDefault();
        $(this).parent("div").css("display", "none").remove();
        var idInput = $(this).parent("div").children(":first").remove();
        $(idInput).attr("value", -$(idInput).attr("value")).remove();
        x--;
    });

});

function registerEdit(index) {
    var element = document.getElementById("group-" + index);
    element.remove();
}