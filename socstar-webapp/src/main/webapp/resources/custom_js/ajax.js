var getAcc = null;
$('#accountSearch').autocomplete({
    minLength: 2,
    source: function (request, response) {
        <!--притаскивать переменную с путем-->
        getAcc = $.get('<c:url value="/searchAcc?filter=" />' + request.term, function (data) {
            response($.map(data, function (seekAccount, i) {
                return {
                    //todo передавать в лэйбле все что нужно завести доп поле и из него вытаскивать
                    value: seekAccount.lastName + " " + seekAccount.firstName,
                    pushToAcc: seekAccount.id,
                    label: seekAccount.lastName + " " + seekAccount.firstName
                }
            }))
        })
    },
    focus: function (event, ui) {
        $('#accountSearch').val(ui.item.label);
        return false;
    },
    select: function (event, ui) {
        location.href = "${pageContext.request.contextPath}/account/user" + ui.item.pushToAcc;
    }
});



    
