package com.getjavajob.konanov.socstar.dao;

import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;

/**
 * Created by RUKONANOVM on 23/06/16.
 */
public interface Dao<T> {
    void create(T value);

    T read(int id) throws ExceptionDao;

    void update(T value);

    void delete(int id);

}
