package com.getjavajob.konanov.socstar.dao;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.FriendRequest;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.*;

/**
 * Created by User on 27.05.2016.
 */

@Repository
public class AccountDaoImpl extends AbstractDao<Account> {

    public AccountDaoImpl() {
        super(Account.class);
    }

    @Override
    public void create(Account value) {
        entityManager.persist(value);
    }

    @Override
    public Account read(int id) {
        return super.read(id);
    }

    @Override
    public void update(Account newInfo) {
        super.update(newInfo);
    }

    @Override
    public void delete(int id) {
        super.delete(id);
    }

    public Account readRegistered(String email, String password) throws ExceptionDao {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> root = cq.from(Account.class);
        cq.select(root).where(cb.equal(root.get("email"), email), cb.equal(root.get("password"), password));
        return entityManager.createQuery(cq).getSingleResult();
    }

    public List<Account> filterByName(String filter) throws ExceptionDao {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> rootEntry = cq.from(Account.class);
        CriteriaQuery<Account> filterQuery = cq.select(rootEntry).where(cb.like(rootEntry.<String>get("lastName"), "%" + filter + "%"));
        TypedQuery<Account> allQuery = entityManager.createQuery(filterQuery);
        return allQuery.getResultList();
    }

    public FriendRequest.Status getFriendsStatus (int yourId, int friendsId) throws ExceptionDao {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<FriendRequest> cq = cb.createQuery(FriendRequest.class);
        Root<FriendRequest> rootEntry = cq.from(FriendRequest.class);
        CriteriaQuery<FriendRequest> filterQuery = cq.select(rootEntry).where(cb.equal(rootEntry.get("from"), yourId),
                cb.equal(rootEntry.get("to"), friendsId));
        FriendRequest friendRequest = entityManager.createQuery(filterQuery).getSingleResult();
        return friendRequest.getStatus();
    }

    public void addFriendship (FriendRequest friendRequest) throws ExceptionDao {
        entityManager.persist(friendRequest);
    }

    @Override
    public void acceptFriendship (int accountId, Integer acceptedId, Integer rejectedId) throws ExceptionDao {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaUpdate<FriendRequest> cq = cb.createCriteriaUpdate(FriendRequest.class);
        Root<FriendRequest> rootEntry = cq.from(FriendRequest.class);
        if (acceptedId != null) {
            cq.set("statusId", 100);
            cq.where(cb.equal(rootEntry.get("from"), acceptedId),
                    cb.equal(rootEntry.get("to"), accountId));
        }
        if (rejectedId != null) {
            cq.set("statusId", 200);
            cq.where(cb.equal(rootEntry.get("from"), rejectedId),
                    cb.equal(rootEntry.get("to"), accountId));
        }
        entityManager.createQuery(cq).executeUpdate();
    }

    @Override
    public List<Account> allAccounts() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<Account> cq = cb.createQuery(Account.class);
        Root<Account> rootEntry = cq.from(Account.class);
        CriteriaQuery<Account> allAccounts = cq.select(rootEntry);
        return entityManager.createQuery(allAccounts).getResultList();
    }
}
