package com.getjavajob.konanov.socstar.dao.connectionpool;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.sql.Connection;

/**
 * Created by User on 12.07.2016.
 */
public class ConnectionProxy implements InvocationHandler {

    private ConnectionPool connectionPool;
    private Connection connection;

    public ConnectionProxy(ConnectionPool connectionPool, Connection connection) {
        this.connectionPool = connectionPool;
        this.connection = connection;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String meth = method.getName();
        if (meth.startsWith("close")) {
            connectionPool.returnConnection((Connection) proxy);
        }
        return method.invoke(connection, args);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ConnectionProxy that = (ConnectionProxy) o;

        if (connectionPool != null ? !connectionPool.equals(that.connectionPool) : that.connectionPool != null)
            return false;
        return connection != null ? connection.equals(that.connection) : that.connection == null;

    }

    @Override
    public int hashCode() {
        int result = connectionPool != null ? connectionPool.hashCode() : 0;
        result = 31 * result + (connection != null ? connection.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ConnectionProxy{" +
                "connectionPool=" + connectionPool +
                ", connection=" + connection +
                '}';
    }
}
