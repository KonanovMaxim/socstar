package com.getjavajob.konanov.socstar.dao.connectionpool;

import java.io.*;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.*;

/**
 * Created by User on 27.05.2016.
 */

public class ConnectionPool {

    private final int MAX_POOL_SIZE = 2;

    private BlockingQueue<Connection> availableConnections = new LinkedBlockingDeque<>(MAX_POOL_SIZE);

    private ThreadLocal<Connection> threadBase = new ThreadLocal<>();
    private ThreadLocal<Integer> connectionsAtDisposal = new ThreadLocal<>();

    private Properties properties = new Properties();
    //todo прикопанный коннекшн

    public ConnectionPool() {
        String[] properties = initPool();
        for (int i = 0; i < MAX_POOL_SIZE; i++) {
            try {
                try {
                    Class.forName("com.mysql.jdbc.Driver");
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                availableConnections.add(DriverManager.getConnection(properties[0], properties[1], properties[2]));
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            //todo достаточно get!=null
            //todo проверить на одновременное вхождение двух потоков
            if (threadBase.get() != null) {
                connectionsAtDisposal.set(connectionsAtDisposal.get() + 1);
                return threadBase.get();
            } else {
                connection = availableConnections.take();
                threadBase.set(connection);
                connectionsAtDisposal.set(1);
                connection.setAutoCommit(false);
                connection = (Connection) Proxy.newProxyInstance(Connection.class.getClassLoader(),
                        new Class[]{Connection.class},
                        new ConnectionProxy(this, connection));
            }
        } catch (InterruptedException | SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public void returnConnection(Connection connection) {
        if (connectionsAtDisposal.get() > 1) {
            connectionsAtDisposal.set(connectionsAtDisposal.get() - 1);
        } else {
            threadBase.remove();
            connectionsAtDisposal.remove();
            try {
                availableConnections.put(connection);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private String[] initPool() {
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream("daoLayer.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String url = properties.getProperty("database.url");
        String user = properties.getProperty("database.user");
        String password = properties.getProperty("database.password");
        return new String[]{url, user, password};
    }
}
