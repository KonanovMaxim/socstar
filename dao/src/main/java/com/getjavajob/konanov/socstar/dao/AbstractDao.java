package com.getjavajob.konanov.socstar.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.Map;

/**
 * Created by User on 13.07.2016.
 */
public abstract class AbstractDao<T> implements Dao<T>, AccountDao<T> {

    @PersistenceContext
    EntityManager entityManager;
    private Class<T> expectedClass;

    public AbstractDao() {
    }

    public AbstractDao(Class<T> expectedClass) {
        this.expectedClass = expectedClass;
    }

    @Override
    @Transactional
    public void create(T value) {
        entityManager.persist(value);
    }

    @Override
    public T read(int id) {
        return entityManager.find(expectedClass, id);
    }

    @Override
    public void update(T value) {
        entityManager.merge(value);
    }

    @Override
    @Transactional
    public void delete(int id) {
        T entity = entityManager.find(expectedClass, id);
        if (entity != null) {
            entityManager.remove(entity);
        }
    }
}
