//package com.getjavajob.konanov.socstar.dao;
//
//import com.getjavajob.konanov.socstar.common.Group;
//import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
//import org.springframework.stereotype.Repository;
//import java.sql.*;
//import java.util.Map;
//
///**
// * Created by User on 04.06.2016.
// */
//
//@Repository
//public class MySQLGroupDao extends AbstractDao<Group> implements Dao<Group> {
//
//    public MySQLGroupDao() {super(Group.class);}
//
//    @Override
//    public void create(Group group) {
//        super.create(group);
//    }
//
//    @Override
//    public Group read(int group_id) {
//        return super.read(group_id);
//    }
//
//    @Override
//    public void update(Group newInfo) {super.update(newInfo);}
//
//    @Override
//    public void delete(int id) {
//        super.delete(id);
//    }
//}
