package com.getjavajob.konanov.socstar.dao;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.FriendRequest;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by RUKONANOVM on 11/08/16.
 */
public interface AccountDao<T> extends Dao<T>  {

    public Account readRegistered(String email, String password) throws ExceptionDao;

    public List<Account> filterByName(String filter) throws ExceptionDao;

    public FriendRequest.Status getFriendsStatus (int yourId, int friendsId) throws ExceptionDao;

    public void addFriendship(FriendRequest friendRequest) throws ExceptionDao;

    public void acceptFriendship(int accountId, Integer acceptedId, Integer rejectedId) throws ExceptionDao;

    public List<Account> allAccounts();
}
