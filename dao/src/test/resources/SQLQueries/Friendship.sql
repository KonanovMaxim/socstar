CREATE TABLE `relationship` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_1` int(10) NOT NULL,
  `user_2` int(10) NOT NULL,
  `statusId` int(11) NOT NULL DEFAULT '300',
  PRIMARY KEY (`id`),
  CONSTRAINT `Relationship_fk0` FOREIGN KEY (`user_1`) REFERENCES `account` (`id`),
  CONSTRAINT `Relationship_fk1` FOREIGN KEY (`user_2`) REFERENCES `account` (`id`)
);


INSERT INTO relationship (id ,user_1, user_2, statusId) VALUES (1, 1, 1, 300);
