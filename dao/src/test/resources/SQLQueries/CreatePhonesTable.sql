CREATE TABLE  IF NOT EXISTS `phones` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `phone` varchar(45) NOT NULL,
  `owner_id` int(10) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`owner_id`) REFERENCES `account` (`id`));