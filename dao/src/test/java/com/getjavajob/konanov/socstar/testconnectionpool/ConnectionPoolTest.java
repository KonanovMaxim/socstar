package com.getjavajob.konanov.socstar.testconnectionpool;

import com.getjavajob.konanov.socstar.dao.connectionpool.ConnectionPool;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * Created by User on 22.06.2016.
 */

public class ConnectionPoolTest {

    private static ConnectionPool connectionPool;

    @Before
    public void createPool() {
            connectionPool = new ConnectionPool();
    }

    @Test
    public void testIfThreadGetsSameConnection() {
        Runnable runnable1 = new Runnable() {
            Connection connection1;
            Connection connection2;
            @Override
            public void run() {
                connection1 = connectionPool.getConnection();
                Connection testConnection1 = getConnection1();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                connection2 = connectionPool.getConnection();
                Connection testConnection2 = getConnection2();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                connectionPool.returnConnection(connection2);
                connectionPool.returnConnection(connection1);
                Assert.assertEquals(testConnection1, testConnection2);
            }

            public Connection getConnection1() {
                return connection1;
            }

            public Connection getConnection2() {
                return connection2;
            }
        };
        Thread thread1 = new Thread(runnable1);
        thread1.run();
        try {
            thread1.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        //todo исправить
    }

    @Test
    public void testIfThreadWaitsWhenNoFreeConnections() throws InterruptedException {
        Thread runnable1 = new Thread() {
            @Override
            public void run() {
                Connection connection = connectionPool.getConnection();
                System.out.println(connection);
            }
        };
        Thread runnable2 = new Thread() {
            @Override
            public void run() {
                Connection connection = connectionPool.getConnection();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println(connection);
                connectionPool.returnConnection(connection);
            }
        };
        Thread runnable3 = new Thread() {
            @Override
            public void run() {
                Connection connection = connectionPool.getConnection();
                System.out.println(connection);
            }
        };
        runnable1.start();
        runnable1.join();
        runnable2.start();
        runnable3.start();
        Thread.sleep(600);
        boolean waiting = runnable3.getState() == Thread.State.WAITING;
        runnable2.join();
        runnable3.join();
        Assert.assertTrue(waiting);
    }

    @Test
    public void testCloseConnection() throws InterruptedException, SQLException {
        Connection connection = connectionPool.getConnection();
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        boolean closed = connection.isClosed();
        System.out.println(closed);
    }
}

//todo перенести в тот же пакет где коннекшн пул
