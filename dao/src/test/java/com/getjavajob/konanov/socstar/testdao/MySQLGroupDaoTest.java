//package com.getjavajob.konanov.socstar.testdao;
//
//import com.getjavajob.konanov.socstar.common.Account;
//import com.getjavajob.konanov.socstar.common.Group;
//import com.getjavajob.konanov.socstar.dao.MySQLAccountDao;
//import com.getjavajob.konanov.socstar.dao.MySQLGroupDao;
//import com.getjavajob.konanov.socstar.dao.connectionpool.ConnectionPool;
//import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
//import org.apache.ibatis.jdbc.ScriptRunner;
//import org.junit.*;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.dao.EmptyResultDataAccessException;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
//
//import java.io.*;
//import java.nio.file.Files;
//import java.nio.file.Paths;
//import java.sql.Connection;
//import java.sql.SQLException;
//
///**
// * Created by User on 18.06.2016.
// */
//
//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(locations = {"classpath:socstar-context-dao.xml", "classpath:socstar-context-overrides.xml"})
//public class MySQLGroupDaoTest {
//    @Autowired
//    private Group group;
//    @Autowired
//    private MySQLGroupDao groupDao;
//    @Autowired
//    private Account owner;
//    @Autowired
//    private MySQLAccountDao accountDao;
//    private static ConnectionPool connectionPool = new ConnectionPool();
//
//
//    @BeforeClass
//    public static void setUpPool() throws FileNotFoundException, SQLException {
//        Connection connection = connectionPool.getConnection();
//        ScriptRunner sc = new ScriptRunner(connection);
//        BufferedReader reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\CreateAccountTable.sql"));
//        sc.runScript(new BufferedReader(reader));
//        reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\CreateGroupTable"));
//        sc.runScript(new BufferedReader(reader));
//        connectionPool.returnConnection(connection);
//    }
//
//    private static void populateAccounts() throws FileNotFoundException, SQLException {
//        Connection connection = connectionPool.getConnection();
//        ScriptRunner sc = new ScriptRunner(connection);
//        BufferedReader reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\PopulateAccounts"));
//        sc.runScript(new BufferedReader(reader));
//        connectionPool.returnConnection(connection);
//    }
//
//    @Test
//    public void testCreateGroup() throws ExceptionDao, IOException, SQLException {
//        populateAccounts();
//        owner = accountDao.read(1);
//        group = new Group("Coffee Shop MSK", owner);
//        group.setId(1);
//        group.setDescription("First Moscow coffee shop.");
//        group.setPhoto(Files.readAllBytes(Paths.get("src\\test\\resources\\picture.jpg")));
//        groupDao.create(group);
//        Group readGroup = groupDao.read(1);
//        Assert.assertEquals(group, readGroup);
//    }
//
//    @After
//    public void tearDown() throws FileNotFoundException, SQLException {
//        Connection connection = connectionPool.getConnection();
//        ScriptRunner sc = new ScriptRunner(connection);
//        BufferedReader reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\DeleteAllGroups.sql"));
//        sc.runScript(new BufferedReader(reader));
//        ScriptRunner sc1 = new ScriptRunner(connection);
//        reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\DeleteAll.sql"));
//        sc1.runScript(new BufferedReader(reader));
//        connectionPool.returnConnection(connection);
//    }
//
//    @Test
//    public void testReadGroup() throws ExceptionDao, FileNotFoundException, SQLException {
//        populateAccounts();
//        populateGroups();
//        owner = accountDao.read(1);
//        group = new Group("Coffee Shop MSK", owner);
//        group.setId(1);
//        group.setDescription("First Moscow coffee shop.");
//        Group readGroup = groupDao.read(1);
//        Assert.assertEquals(readGroup, group);
//    }
//
//    private void populateGroups() throws FileNotFoundException, SQLException {
//        Connection connection = connectionPool.getConnection();
//        ScriptRunner sc = new ScriptRunner(connection);
//        BufferedReader reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\PopulateGroups"));
//        sc.setAutoCommit(true);
//        sc.runScript(new BufferedReader(reader));
//        connectionPool.returnConnection(connection);
//    }
//
//    @Test
//    public void testUpdateGroup() throws IOException, ExceptionDao, SQLException {
//        populateAccounts();
//        populateGroups();
//        owner = accountDao.read(1);
//        group = new Group("Coffee Shop MSK121212", owner);
//        group.setId(1);
//        group.setDescription("First Moscow coffee shop121212.");
//        groupDao.update(group);
//        Group readGroup = groupDao.read(1);
//        Assert.assertEquals(readGroup, group);
//    }
//
//    @Test(expected = EmptyResultDataAccessException.class)
//    public void testDeleteGroup() throws IOException, ExceptionDao, SQLException {
//        groupDao.delete(1);
//        groupDao.read(1);
//    }
//}
