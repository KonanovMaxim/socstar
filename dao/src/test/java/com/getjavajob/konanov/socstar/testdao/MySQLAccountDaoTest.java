package com.getjavajob.konanov.socstar.testdao;

import com.getjavajob.konanov.socstar.common.Account;
import com.getjavajob.konanov.socstar.common.FriendRequest;
import com.getjavajob.konanov.socstar.common.Phone;
import com.getjavajob.konanov.socstar.dao.AccountDaoImpl;
import com.getjavajob.konanov.socstar.dao.connectionpool.ConnectionPool;
import com.getjavajob.konanov.socstar.dao.exceptionsdao.ExceptionDao;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.*;

/**
 * Created by User on 04.06.2016.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:socstar-context-dao.xml", "classpath:socstar-context-overrides.xml"})
public class MySQLAccountDaoTest {

    @Autowired
    private AccountDaoImpl accountDao;
    @Autowired
    private Account testAccount;
    private static ConnectionPool connectionPool = new ConnectionPool();

    @BeforeClass
    public static void setUpPool() throws FileNotFoundException, SQLException {
        Connection connection = connectionPool.getConnection();
        ScriptRunner sc = new ScriptRunner(connection);
        sc.runScript(new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\CreateAccountTable.sql")));
        sc.runScript(new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\PopulateAccounts")));
        sc.runScript(new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\CreatePhonesTable.sql")));
        connectionPool.returnConnection(connection);
    }

    private static void populatePhones() throws FileNotFoundException, SQLException {
        Connection connection = connectionPool.getConnection();
        ScriptRunner sc = new ScriptRunner(connection);
        sc.runScript(new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\PopulatePhones.sql")));
        connectionPool.returnConnection(connection);
    }

    private static void doFriends() throws FileNotFoundException, SQLException {
        Connection connection = connectionPool.getConnection();
        ScriptRunner sc = new ScriptRunner(connection);
        sc.runScript(new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\Friendship.sql")));
        connectionPool.returnConnection(connection);
    }

    @Test
    @Transactional
    public void testCreateAccount() throws ExceptionDao, IOException, SQLException {
        testAccount = new Account("password1234", "ivan@gmail.com", "Ivanov");
        testAccount.setFirstName("Ivan");
        testAccount.setMiddleName("Ivanovich");
        testAccount.setSkype("ivanSkype");
        testAccount.setIcq("89213743238");
        testAccount.setPhoto(Files.readAllBytes(Paths.get("src\\test\\resources\\picture.jpg")));

        Phone phone1 = new Phone("+73335566926", testAccount);
        Phone phone2 = new Phone("+73435234", testAccount);
        ArrayList<Phone> phones = new ArrayList<>();
        phones.add(phone1);
        phones.add(phone2);

        testAccount.setPhones(phones);

        accountDao.create(testAccount);
        Account readAccount = accountDao.read(2);
        Assert.assertEquals(readAccount,testAccount);
    }

    @After
    public void tearDown() throws Exception {
        Connection connection = connectionPool.getConnection();
        ScriptRunner sc = new ScriptRunner(connection);
        BufferedReader reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\DeleteAll.sql"));
        sc.runScript(new BufferedReader(reader));
        connectionPool.returnConnection(connection);
    }

    @Test
    @Transactional
    public void testReadAccount() throws ExceptionDao, FileNotFoundException, SQLException, ParseException {
        populateAccounts();
        populatePhones();
        testAccount = new Account("password", "johns@gmail.com", "Johns");
        testAccount.setId(1);
        testAccount.setFirstName("Paul");
        testAccount.setMiddleName("John");
        testAccount.setSkype("834928");
        testAccount.setIcq("1237894");
        testAccount.setEnrollmentDate(new Date());
        Phone phone1 = new Phone("+79169238500", testAccount);
        phone1.setId(1);
        Phone phone2 = new Phone("+73335566926", testAccount);
        phone2.setId(2);
        Phone phone3 = new Phone("+73335566926", testAccount);
        phone3.setId(3);
        ArrayList<Phone> phones = new ArrayList<>();
        phones.add(phone1);
        phones.add(phone2);
        phones.add(phone3);

        testAccount.setPhones(phones);

        Account readAccount = accountDao.read(1);
        Assert.assertEquals(testAccount, readAccount);
    }

    private void populateAccounts() throws FileNotFoundException, SQLException {
        ScriptRunner sc = new ScriptRunner(connectionPool.getConnection());
        BufferedReader reader = new BufferedReader(new FileReader("src\\test\\resources\\SQLQueries\\PopulateAccounts"));
        sc.runScript(new BufferedReader(reader));
    }

    @Test
    @Transactional
    public void testUpdateAccount() throws ExceptionDao, FileNotFoundException, SQLException {
        populateAccounts();
        populatePhones();
        testAccount = new Account("password12", "johns12@gmail.com", "Johns12");
        testAccount.setId(1);
        testAccount.setFirstName("Paul12");
        testAccount.setMiddleName("John12");
        testAccount.setSkype("83492812");
        testAccount.setIcq("123789412");
        testAccount.setFriendsOut(null);
        accountDao.update(testAccount);
        Account readAccount = accountDao.read(1);
        Assert.assertEquals(readAccount, testAccount);
    }

    @Test
    @Transactional
    public void testDeleteAccount() throws ExceptionDao, FileNotFoundException, SQLException {
        accountDao.delete(1);
        Assert.assertNull(accountDao.read(1));
    }

    @Test
    @Transactional
    public void Friends() throws ExceptionDao, FileNotFoundException, SQLException {
        populateAccounts();
        populatePhones();
        doFriends();
        Account account = accountDao.read(1);
        List<Account> friendRequests = account.getFriendsOut();
        friendRequests.size();
    }
}
