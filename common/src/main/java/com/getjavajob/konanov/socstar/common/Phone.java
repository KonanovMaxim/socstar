package com.getjavajob.konanov.socstar.common;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.springframework.stereotype.Component;

import javax.persistence.*;

/**
 * Created by User on 29.06.2016.
 */

@Component
@Entity
@Table(name = "phones")
public class Phone {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String phone;

    @ManyToOne
    @JoinColumn(name = "owner_id")
    private Account owner;

    public Phone(String phone, Account owner) {
        this.phone = phone;
        this.owner = owner;
    }

    public Phone() {
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JsonBackReference
    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Phone phone1 = (Phone) o;

        if (getId() != phone1.getId()) return false;
        return (getPhone() != null ? getPhone().equals(phone1.getPhone()) : phone1.getPhone() != null);

    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getPhone() != null ? getPhone().hashCode() : 0);
        return result;
    }
}
