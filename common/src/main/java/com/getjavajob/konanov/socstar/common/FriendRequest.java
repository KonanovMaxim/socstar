package com.getjavajob.konanov.socstar.common;

import javax.persistence.*;

/**
 * Created by RUKONANOVM on 29/06/16.
 */

@Entity
@Table(name = "relationship")
public class FriendRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "user_1")
    private int from;
    @Column(name = "user_2")
    private int to;
    @Column(name = "statusId")
    private int statusId;

    public FriendRequest() {
    }

    public FriendRequest(int from, int to, int statusId) {
        this.from = from;
        this.to = to;
        this.statusId = statusId;
    }

    public FriendRequest(int from, int to) {
        setStatus(Status.WAITING);
        this.from = from;
        this.to = to;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getFrom() {
        return from;
    }

    public void setFrom(int from) {
        this.from = from;
    }

    public int getTo() {
        return to;
    }

    public void setTo(int to) {
        this.to = to;
    }

    public Status getStatus() {
        return Status.parse(this.statusId);
    }

    public void setStatus(Status status) {
        this.statusId = status.getValue();
    }

    public enum Status {
        CONFIRMED(100), REJECTED(200), WAITING(300);

        private int value;

        Status(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }

        public static Status parse(int id) {
            Status status = null; // Default
            for (Status item : Status.values()) {
                if (item.getValue() == id) {
                    status = item;
                    break;
                }
            }
            return status;
        }
    }
}
