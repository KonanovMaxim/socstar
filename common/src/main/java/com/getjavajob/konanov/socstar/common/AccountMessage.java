package com.getjavajob.konanov.socstar.common;

import java.util.Date;

/**
 * Created by RUKONANOVM on 29/06/16.
 */

public class AccountMessage {
    private int id;
    private Account sender;
    private Account receiver;
    private Date sendDate;
    private String messageContent;
    private byte[] picture;
    private MessageType messageType;

    public AccountMessage(Account sender, Account receiver, Date sendDate, String messageContent, MessageType messageType) {
        this.sender = sender;
        this.receiver = receiver;
        this.sendDate = sendDate;
        this.messageContent = messageContent;
        this.messageType = messageType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getSender() {
        return sender;
    }

    public void setSender(Account sender) {
        this.sender = sender;
    }

    public Account getReceiver() {
        return receiver;
    }

    public void setReceiver(Account receiver) {
        this.receiver = receiver;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }

    public String getMessageContent() {
        return messageContent;
    }

    public void setMessageContent(String messageContent) {
        this.messageContent = messageContent;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(MessageType messageType) {
        this.messageType = messageType;
    }

    public enum MessageType {
        WALL, PRIVATE
    }
}
