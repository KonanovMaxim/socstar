package com.getjavajob.konanov.socstar.common;

import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by User on 04.06.2016.
 */

@Component
public class Group {

    private int id;
    private String name;
    private Account owner;
    private Date enrollmentDate;
    private String description;
    private List<Account> subscribers = new ArrayList<>();
    private List<Account> admins = new ArrayList<>();
    private byte[] photo;

    public Group() {
        this.enrollmentDate = getCurrentTimeStamp();
    }

    public Group(String name, Account owner) {
        this.name = name;
        this.owner = owner;
        this.enrollmentDate = getCurrentTimeStamp();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account ownerId) {
        this.owner = ownerId;
    }

    public Date getEnrollmentDate() {
        return getCurrentTimeStamp();
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = getCurrentTimeStamp();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private static Date getCurrentTimeStamp() {
        return new Date(new java.util.Date().getTime());
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public List<Account> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<Account> subscribers) {
        this.subscribers = subscribers;
    }

    public List<Account> getAdmins() {
        return admins;
    }

    public void setAdmins(List<Account> admins) {
        this.admins = admins;
    }

    public String getDateRepresentation() {
        return new SimpleDateFormat("YYYY-MM-DD").format(enrollmentDate.getTime());
    }

    //@Override
    //public boolean equals(Object obj) {
    //    if (this == obj) return true;
    //    if (!(obj instanceof Group)) return false;
//
    //    Group group = (Group) obj;
    //    if (id != group.getId()) return false;
    //    if (!name.equals(group.getName())) return false;
    //    if (!owner.equals(group.getOwner())) return false;
    //    System.out.println(description);
    //    System.out.println(group.getDescription());
    //    if (description != null ? !description.equals(group.getDescription()) : group.description != null) return false;
    //    if (!getDateRepresentation().equals(group.getDateRepresentation())) return false;
    //    //for (int i = 0; i < subscribers.size(); i++) {
    //    //    return (subscribers.get(i) != null && subscribers.get(i).equals(group.subscribers.get(i)) && group.subscribers.get(i) != null);
    //    //}
    //    //for (int i = 0; i < admins.size(); i++) {
    //    //    return (admins.get(i) != null && admins.get(i).equals(group.admins.get(i)) && group.admins.get(i) != null);
    //    //}
    //    return !(photo != null ? !Arrays.equals(photo, group.getPhoto()) : group.photo != null);
    //}
//
    //@Override
    //public int hashCode() {
    //    int result = id;
    //    result = 31 * result + name.hashCode();
    //    result = 31 * result + owner.hashCode();
    //    result = 31 * result + enrollmentDate.hashCode();
    //    result = 31 * result + (description != null ? description.hashCode() : 0);
    //    result = 31 * result + Arrays.hashCode(photo);
    //    return result;
    //}
}
