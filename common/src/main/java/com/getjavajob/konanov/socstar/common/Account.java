package com.getjavajob.konanov.socstar.common;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.hibernate.annotations.WhereJoinTable;

import javax.persistence.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by User on 28.05.2016.
 */

@Entity
@Table(name = "account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "middle_name")
    private String middleName;

    @Column(name = "last_name")
    private String lastName;

    private String email;

    private String password;

    private String skype;

    private String icq;

    @Column(name = "enrollment_date")
    @Temporal(TemporalType.DATE)
    private Date enrollmentDate = new Date();

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JsonManagedReference
    private List<Phone> phones = new ArrayList<>();

    private byte[] photo;

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinTable(name = "relationship", joinColumns = @JoinColumn(name = "user_1", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "user_2", referencedColumnName = "id"))
    @WhereJoinTable(clause = "statusId=100")
    private List<Account> friendsOut = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinTable(name = "relationship", joinColumns = @JoinColumn(name = "user_2", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_1", referencedColumnName = "id"))
    @WhereJoinTable(clause = "statusId=100")
    private List<Account> friendsIn = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinTable(name = "relationship", joinColumns = @JoinColumn(name = "user_2", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_1", referencedColumnName = "id"))
    @WhereJoinTable(clause = "statusId=300")
    private List<Account> requests = new ArrayList<>();

    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JsonIgnore
    @JoinTable(name = "relationship", joinColumns = @JoinColumn(name = "user_1", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_2", referencedColumnName = "id"))
    @WhereJoinTable(clause = "statusId=300")
    private List<Account> requestsFromMe = new ArrayList<>();

    public List<Account> getFriendsOut() {
        return friendsOut;
    }


    public List<Account> getFriendsIn() {
        return friendsIn;
    }

    public Account(String password, String email, String lastName) {
        this.enrollmentDate = getCurrentTimeStamp();
        this.password = password;
        this.email = email;
        this.lastName = lastName;
    }

    public List<Account> getRequestsFromMe() {
        return requestsFromMe;
    }

    public void setRequestsFromMe(List<Account> requestsFromMe) {
        this.requestsFromMe = requestsFromMe;
    }

    public void setFriendsOut(List<Account> friendsOut) {
        this.friendsOut = friendsOut;
    }

    public void setFriendsIn(List<Account> friendsIn) {
        this.friendsIn = friendsIn;
    }

    public void setRequests(List<Account> requests) {
        this.requests = requests;
    }

    public List<Account> getRequests() {
        return requests;
    }


    public Account(String email) {
        this.enrollmentDate = getCurrentTimeStamp();
        this.email = email;
    }

    public Account() {
        this.enrollmentDate = getCurrentTimeStamp();
    }

    public int getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getSkype() {
        return skype;
    }

    public String getIcq() {
        return icq;
    }

    public String getDateRepresentation() {
        return new SimpleDateFormat("YYYY-MM-DD").format(enrollmentDate.getTime());
    }

    public Date
    getEnrollmentDate() {
        return enrollmentDate;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public void setEnrollmentDate(Date enrollmentDate) {
        this.enrollmentDate = enrollmentDate;
    }

    public void setPhoto(byte[] photo) throws FileNotFoundException {
        this.photo = photo;
    }

    public void setId(int id) {
        this.id = id;
    }

    private static Date getCurrentTimeStamp() {
        return new Date(new java.util.Date().getTime());
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void addPhone(Phone phone) {
        this.phones.add(phone);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Account account = (Account) o;

        if (getId() != account.getId()) return false;
        if (getFirstName() != null ? !getFirstName().equals(account.getFirstName()) : account.getFirstName() != null)
            return false;
        if (getMiddleName() != null ? !getMiddleName().equals(account.getMiddleName()) : account.getMiddleName() != null)
            return false;
        if (getLastName() != null ? !getLastName().equals(account.getLastName()) : account.getLastName() != null)
            return false;
        if (getEmail() != null ? !getEmail().equals(account.getEmail()) : account.getEmail() != null) return false;
        if (getPassword() != null ? !getPassword().equals(account.getPassword()) : account.getPassword() != null)
            return false;
        if (getSkype() != null ? !getSkype().equals(account.getSkype()) : account.getSkype() != null) return false;
        if (getIcq() != null ? !getIcq().equals(account.getIcq()) : account.getIcq() != null) return false;
        if (getDateRepresentation() != null ? !getDateRepresentation().equals(account.getDateRepresentation()) : account.getDateRepresentation() != null)
            return false;
        if (phones.size() != account.phones.size()) return false;
        for (int i = 0; i < phones.size(); i++) {
            if (!phones.get(i).equals(account.getPhones().get(i))) return false;
        }
        return (Arrays.equals(getPhoto(), account.getPhoto()));
    }

    @Override
    public int hashCode() {
        int result = getId();
        result = 31 * result + (getFirstName() != null ? getFirstName().hashCode() : 0);
        result = 31 * result + (getMiddleName() != null ? getMiddleName().hashCode() : 0);
        result = 31 * result + (getLastName() != null ? getLastName().hashCode() : 0);
        result = 31 * result + (getEmail() != null ? getEmail().hashCode() : 0);
        result = 31 * result + (getPassword() != null ? getPassword().hashCode() : 0);
        result = 31 * result + (getSkype() != null ? getSkype().hashCode() : 0);
        result = 31 * result + (getIcq() != null ? getIcq().hashCode() : 0);
        result = 31 * result + (getEnrollmentDate() != null ? getEnrollmentDate().hashCode() : 0);
        result = 31 * result + (getPhones() != null ? getPhones().hashCode() : 0);
        result = 31 * result + Arrays.hashCode(getPhoto());
        result = 31 * result + (friendsOut != null ? friendsOut.hashCode() : 0);
        return result;
    }
}
